#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    fileCounter = 0;
    mappingMode = 0;
    //setInputFile("/Users/Shared/Max 7/Examples");
    setInputFile("/Users/ryancashman/Documents/of_v0.9.3_osx_release/examples/3d/3DPrimitivesExample");
    //setInputFile("/Users/ryancashman/Music/iTunes/iTunes Media");
    //setInputFile("/Users/ryancashman/Music/Ableton/User Library");
    folderArcScale = fileArcScale = 0.3;
    ofBackground(64);
}

//--------------------------------------------------------------
void ofApp::update(){
 //   for (int i = 0; i < sunburst.size(); i++)
   // {
     //   cout<<"update index:     "<<i<<endl;
     //   sunburst[i].update(0);
   // }
}

//--------------------------------------------------------------
void ofApp::draw(){
    if (isSavingPDF)
    {     
        ofBeginSaveScreenAsPDF("screenshot-"+ofGetTimestampString()+".pdf", false);
    }
    //cout << sunburst.size() << endl;
    for (int i = 0; i < sunburst.size(); i++)
    {
        sunburst[i].drawArc(folderArcScale, fileArcScale);
    }
    if (isSavingPDF)
    {
        cout << "save!" << endl;
        ofEndSaveScreenAsPDF();
        isSavingPDF = false;
    }    
}

void ofApp::setInputFile(string path)
{
    ofFile startFile = ofFile(path);
    fileSys.setup(startFile);
    //fileSys.printDepthFirst(0,0);
    
    //init sunburst
    sunburst = fileSys.createSunburstItems();
    fileSizeMin = fileSizeMax = 0;
    //cout<<"no of sunburst items: " << sunburst.size() <<endl;
    for (int i = 1; i < sunburst.size();  i++)
    {
        double _fileSize = sunburst[i].fileSize;
        if (i == 1)
        {
            fileSizeMin = _fileSize;
        }
        if (sunburst[i].file.isFile())
        {
            fileSizeMin = MIN(_fileSize , fileSizeMin);
            fileSizeMax = MAX(_fileSize , fileSizeMax);
        }
        depthMax = MAX(depthMax, sunburst[i].depth);
    }
    fileSys.depthMax = depthMax;
   // fileSys.myOfApp = this;
    cout<<"depthMax:    "<<depthMax<<endl;
    cout<<"fileSizeMin:    "<<fileSizeMin<<endl;
    cout<<"fileSizeMax:    "<<fileSizeMax<<endl;
    for (int i = 0; i < sunburst.size(); i ++)
    {
      //  sunburst[i].myOfApp = this;
        sunburst[i].sunburst = & sunburst;
        sunburst[i].depthMax = depthMax;
        sunburst[i].fileMinSize = fileSizeMin;
        sunburst[i].fileMaxSize = fileSizeMax;        
        sunburst[i].update(mappingMode);
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if(key=='s')
    {
         isSavingPDF = true;
        ofImage screenShot;
        screenShot.grabScreen(0, 0, ofGetWidth(), ofGetHeight());
        screenShot.save("screenshot-"+ofGetTimestampString()+".png");
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 
    
}
