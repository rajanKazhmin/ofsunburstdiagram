//
//  FileSystemItem.cpp
//  myFilesAndFolders
//
//  Created by Ryan Cashman on 6/26/16.
//
//

#include "FileSystemItem.h"

FileSystemItem::FileSystemItem()
{
}

//--------------------------------------------------------------

void FileSystemItem::setup(ofFile file)
{
    this->file = file;
    if (file.isDirectory()==true && file.getExtension() != "app")
    {
        ofDirectory dir = ofDirectory(file.getAbsolutePath());
        dir.sort();
        int size = dir.listDir();
        for (int i = 0; i < size; i++)
        {
            if (dir.getFile(i).exists() == 0)
            {
                continue;
            }
            ofFile childFile = ofFile(dir.getFile(i).getAbsolutePath());
            FileSystemItem child;
            child.setup(childFile);
            children.push_back(child);
        }
    }
}

//--------------------------------------------------------------


void FileSystemItem::printDepthFirst(int depth, int indexToParent)
{
    for(int i = 0; i < depth; i++)
    {
        cout << "    ";
    }
    cout << fileCount << " " << indexToParent <<  " <--> " << "(" << depth << ")" << file.getFileName() << endl;
    indexToParent = fileCount;
    fileCount++;
    for (int j = 0; j < children.size(); j++)
    {
        children[j].printDepthFirst(depth+1, indexToParent);
    }
}

//--------------------------------------------------------------

void FileSystemItem::update()
{
    
}

//--------------------------------------------------------------

void FileSystemItem::draw()
{
    
}

//--------------------------------------------------------------

vector <SunburstItem> FileSystemItem::createSunburstItems()
{
    float megabytes = this->getFileSize();
    float anglePerMegabyte = ofRadToDeg(TWO_PI/megabytes);
    //anglePerMegabyte =
    vector<FileSystemItem> items;
    vector<int> depths;
    vector<int> indicesParent;
    vector<SunburstItem> sunburstItems;
    vector <float> angles;
    
    items.push_back(* this);
    depths.push_back(0);
    indicesParent.push_back(-1);
    angles.push_back(0.0);
    
    int index = 0;
    float angleOffset = 0, oldAngle = 0;
    
    while (items.size() > index)
    {
        FileSystemItem item = items[index];
        int depth = depths[index];
        int indexToParent = indicesParent[index];
        float angle = angles[index];
        
        if (oldAngle != angle) { angleOffset = 0.0; }
        if (item.file.isDirectory() == 1)
        {

            for (int i = 0; i < item.children.size(); i++)
            {
                items.push_back(item.children[i]);
                depths.push_back(depth+1);
                indicesParent.push_back(index);
                angles.push_back(angle+angleOffset);
            }
        }
        
        SunburstItem sb = SunburstItem();
        float newAngle = ofRadToDeg(fmod(angle+angleOffset, TWO_PI));
        float _fileSize = item.getFileSize();

        sb.setup(item.file, index, depth, depthMax, indexToParent, item.childCount, _fileSize, item.fileSizeMin, item.fileSizeMax, fileSizeMin, fileSizeMax, newAngle, _fileSize*anglePerMegabyte);
        
        sunburstItems.push_back(sb);
        
        angleOffset += ofRadToDeg(item.getFileSize()*anglePerMegabyte);
        index++; // DONT FORGET TO INCREMENT, this is a while loop
        oldAngle = angle;
    }
    return sunburstItems;
}

//--------------------------------------------------------------

double FileSystemItem::getFileSize()
{
    float MEGABYTE = 1048576;
    double _fileSize = getFileSize(* this) / MEGABYTE;
    return _fileSize;
}

//--------------------------------------------------------------

long FileSystemItem::getFileSize(FileSystemItem hdItem)
{
    if (hdItem.file.isFile() == 1)
        {
            long hdItem_size = hdItem.file.getSize();
            return hdItem_size;
        }
    vector <FileSystemItem> hdItems = hdItem.children;
    long totalFileSize = 0;
    if (hdItems.size() > 0)
    {
        for (int i = 0; i < hdItems.size(); i++)
        {
            totalFileSize+= getFileSize(hdItems[i]);
        }
        return totalFileSize;
    }
}

//--------------------------------------------------------------