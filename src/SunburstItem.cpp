//
//  SunburstItem.cpp
//  ofSunburstDiagram
//
//  Created by Ryan Cashman on 7/8/16.
//
//

#include "SunburstItem.h"

//--------------------------------------------------------------

void SunburstItem::setup(ofFile file, int index, int depth, int depthMax, int indexToParent, int childCount, double fileSize, double folderMinFileSize, double folderMaxFileSize, double fileMinSize, double fileMaxSize, float angle, float extension)
{
    //cout<<"setup sunburst item"<<endl;
    this->file = file;
    this->index = index;
    this->depth = depth;
    this->depthMax = depthMax;
    this->indexToParent = indexToParent;
    this->childCount = childCount;
    this->fileSize = fileSize;
    this->folderMinFileSize = folderMinFileSize;    //
    this->folderMaxFileSize = folderMaxFileSize;    //
    this->fileMinSize = fileMinSize;                //
    this->fileMaxSize = fileMaxSize;                //
    this->angleStart = angle;
    this->angleEnd = angle + extension;
    this-> angleCenter = angleEnd/2;
    this->extension = extension;
    this->folderBrightnessStart = 122;
    this->folderBrightnessEnd = 25;
    //cout<<"FileMinSize:     "<< fileMinSize << "FileMaxSize     " << fileMaxSize << endl;
    //this->radius = calcEqualAreaRadius(depth, depthMax);
    //this->depthWeight = calcEqualAreaRadius(depth+1, depthMax) - radius;
}

//--------------------------------------------------------------

void SunburstItem::update(int mappingMode)
{
    radius = calcEqualAreaRadius(depth, depthMax);
    depthWeight = calcEqualAreaRadius(depth+1, depthMax) - radius;
    if (indexToParent > -1)
    {
        x = cos(angleCenter) * radius;
        y = sin(angleCenter) * radius;
        
        c1x = cos(angleCenter);
        c1x *= calcEqualAreaRadius(depth-1, depthMax);
        
        c1y = sin(angleCenter);
        c1y *= calcEqualAreaRadius(depth-1, depthMax);
        
        // need a link in sunburst items and filesystem items back to mainapp!!!!
        // too many band aids with passing down depthMax, filesize min/max etc...
        //  clean up!!!
        
        vector<SunburstItem> sb = *sunburst;
        SunburstItem parentSunburst = sb[indexToParent];
        c2x = cos(parentSunburst.angleCenter);
        c2x *= calcEqualAreaRadius(depth-1, depthMax);
    
        c2y = sin(parentSunburst.angleCenter);
        c2y *= calcEqualAreaRadius(depth-1, depthMax);
        
        //chord
        float startX = cos(angleStart) + radius;
        float startY = sin(angleStart) * radius;
        float endX = cos(angleEnd) * radius;
        float endY = sin(angleEnd) * radius;
        arcLength = ofDist(startX, startY, endX, endY);
        
        //mapping mode goes here...
        //
  
        double percent = 0;
        percent = ofNormalize(fileSize, fileMinSize, fileMaxSize);
        
        if (isDir)
        {
            folderBrightnessStart = 55;
            folderBrightnessEnd = 225;
            float bright = ofLerp(folderBrightnessStart, folderBrightnessEnd, percent);
            color = ofColor(0,0, bright, 255);
        }
        else
        {
            ofColor from = ofColor(255,0,0,255);
            ofColor to = ofColor(0,255,255, 255);
            color = from.lerp(to, percent);
        }
    }
}

//--------------------------------------------------------------

void SunburstItem::drawArc(float folderScale, float fileScale)
{
    float arcRadius;
    if (depth > 0)
    {
        float lineWidth;
        if (isDir == 1)
        {
            lineWidth = (depthWeight * folderScale);
            arcRadius = radius + ((depthWeight*folderScale)/2);
        }
        else {
            lineWidth = (depthWeight * fileScale);
            arcRadius = radius + ((depthWeight * fileScale)/2);
        }
       // ofNoFill();
       // ofSetColor(color);
        //ofPolyline arcPolyLine;s
        ofPath arcPath;
        ofPoint p = ofPoint(ofGetWidth()/2,ofGetHeight()/2);
        angleStart = floor(angleStart);
        angleEnd = ceil(angleEnd);
        if (angleStart<angleEnd)
        {
        arcPath.arc(p, arcRadius, arcRadius, angleStart, angleEnd);
        arcPath.arcNegative(p, arcRadius+lineWidth, arcRadius+lineWidth, angleEnd, angleStart);
        arcPath.close();
        arcPath.setCircleResolution(60);
        arcPath.setFillColor(color);
        arcPath.setFilled(true);
        arcPath.draw();}
    }
}

//--------------------------------------------------------------

float SunburstItem::calcEqualAreaRadius(int depth, int depthMax)
{
    //cout << "Begin!  equalAreaRadius" << endl;
    float equalAreaRadius = sqrt(depth * pow(ofGetHeight()/2, 2) / (depthMax+1));
    //cout << "equalAreaRadius:    " << equalAreaRadius << endl;
    return equalAreaRadius;
}