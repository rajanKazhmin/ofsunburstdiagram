//
//  SunburstItem.h
//  ofSunburstDiagram
//
//  Created by Ryan Cashman on 7/8/16.
//
//

#ifndef SunburstItem_h
#define SunburstItem_h

#include "ofMain.h"
//#include "ofApp.h"

class SunburstItem
{
public:
    
    ofFile file;
    //ofApp * myOfApp;
    
    vector<SunburstItem> * sunburst;
    
    // relations
    int index;
    int depth, depthMax;
    int indexToParent;
    int childCount;
    
    // meta file info
    string name;
    bool isDir;
    //int lastModified; // age of file in days
    double fileSize, folderMinFileSize, folderMaxFileSize;
    float fileMinSize, fileMaxSize;
    
    ofColor color;
    float lineWeight;
    float angleStart, angleCenter, angleEnd;
    float extension;
    float radius;
    float depthWeight;
    float x, y;
    float arcLength;
    
    float c1x, c1y, c2x, c2y;
    
    float folderBrightnessStart;
    float folderBrightnessEnd;
    
    void setup(ofFile file, int index, int depth, int depthMax, int indexToParent, int childCount, double fileSize, double folderMinFileSize, double folderMaxFileSize, double fileMinSize, double fileMaxSize, float angle, float extension);
    void update (int mappingMode);
    void drawArc (float folderScale, float fileScale);
    float calcEqualAreaRadius(int depth, int depthMax);
    void drawRelationLine();
    void drawRelationBezier();
    
};

#endif /* SunburstItem_h */
