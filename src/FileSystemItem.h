//
//  FileSystemItem.h
//  myFilesAndFolders
//
//  Created by Ryan Cashman on 6/26/16.
//
//

#ifndef FileSystemItem_h
#define FileSystemItem_h

#include "ofMain.h"
#include "SunburstItem.h"
//#include "ofApp.h"

class FileSystemItem
{
public:
   // ofApp * myOfApp;
    
    void setup(ofFile file);
    void update();
    void draw();
    void printDepthFirst(int depth, int indexToParent);
    
    double getFileSize();
    long getFileSize(FileSystemItem hdItem);
    vector <SunburstItem> createSunburstItems();
    
    ofFile file;
    //ofDirectory dir;
    vector <FileSystemItem> children;
    //FileSystemItem children[];
    int childCount;
    int fileCount;
    double folderMinFileSize, folderMaxFileSize, fileSizeMin, fileSizeMax;
    int depthMax;
    
    FileSystemItem();
private:
    
};

#endif /* FileSystemItem_h */
